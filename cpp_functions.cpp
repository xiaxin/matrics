#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
DataFrame conversion_function(CharacterVector uid,
                              IntegerVector age,
                              IntegerVector max_age) 
{
  int rows = age.size();
  int m_age = max_age[0];
  
  IntegerVector age_out(m_age + 1, NA_INTEGER);
  IntegerVector cumulative_users(m_age + 1, NA_INTEGER);

  for (int a = 0; a <= m_age ; a++) {
    std::set<std::string> users; 
    age_out[a] = a;
    for (int r = 0; r < rows; r++) {
      std::string uid_string = Rcpp::as<std::string>(uid[r]); 
      if (age[r] <= a) users.insert(uid_string); 
    }
    cumulative_users[a] = users.size();
  }
  
  // return a data frame of the results
  return DataFrame::create(Named("age") = age_out, 
                           Named("monetizers") = cumulative_users);
}

